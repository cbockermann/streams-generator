package streams.generator.correlated;

import static org.junit.Assert.*;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class EsfanahiStreamTest {
	
	private final static Logger log = LoggerFactory.getLogger(EsfanahiStreamTest.class);

	/**
	 * Tests wether the true covariance matrix can be resembled by generated
	 * data.
	 */
	@Test
	public void test() throws Exception {
		
		final int label = 1;
		final double epsilon = 0.1;	// max distance between computed and actual values
		
		CorrelatedGaussianStream s = new EsfanahiStream(false, label);
		s.init();
		
		// compute covariance
		double[][] exMatrix = new double[10000][15];	// examples
		for (int i = 0; i < exMatrix.length; i++) {
			Data item = s.read();
			for (int j = 0; j < 15; j++)
				exMatrix[i][j] = (double) item.get("att"+String.format("%02d", j+1));
		}
		RealMatrix computed = new Covariance(exMatrix).getCovarianceMatrix();
		log.info("Covariance (computed, {}x{}):\n{}",
				computed.getRowDimension(), computed.getColumnDimension(),
				computed.toString());
		
		// compare to truth
		RealMatrix truth = s.getClassStructures().get(Integer.toString(label)).cov;
		double maxDiff = Double.MIN_VALUE;
		for (int row = 0; row < truth.getRowDimension(); row++)
			for (int col = 0; col < truth.getColumnDimension(); col++) {
				double v_t = truth.getEntry(row, col);
				double v_c = computed.getEntry(row, col);
				maxDiff = Math.max(maxDiff, Math.abs(v_t - v_c));
				assertEquals(v_t, v_c, epsilon);
			}
		
		log.info("Matrices almost equal (epsilon = {}, max difference = {}). Test passed.",
				epsilon, maxDiff);
		
		s.close();
		
	}

}
