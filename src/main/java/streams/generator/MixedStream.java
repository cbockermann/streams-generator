/**
 * 
 */
package streams.generator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import stream.Data;
import stream.data.DataFactory;
import stream.io.Stream;

/**
 * @author chris
 *
 */
public class MixedStream extends GeneratorDataStream {

    Map<String, Class<?>> types = new LinkedHashMap<String, Class<?>>();
    Double totalWeight = 0.0d;
    List<Double> weights = new ArrayList<Double>();
    List<Stream> streams = new ArrayList<Stream>();

    Random rnd = new Random();

    /**
     * @see stream.io.DataStream#getAttributes()
     */
    public Map<String, Class<?>> getAttributes() {
        return types;
    }

    public void add(Double weight, Stream stream) {
        streams.add(stream);
        weights.add(totalWeight + weight);
        // types.putAll(stream.getAttributes());
        totalWeight += weight;
    }

    protected int choose() {

        double d = rnd.nextDouble();
        Double t = d * totalWeight;

        for (int i = 0; i < weights.size(); i++) {
            if (i + 1 < weights.size() && weights.get(i + 1) > t)
                return i;
        }

        return weights.size() - 1;
    }

    /**
     * @see stream.io.DataStream#readNext()
     */
    public Data readNext() throws Exception {
        return readNext(DataFactory.create());
    }

    /**
     * @see stream.io.DataStream#readNext(stream.data.Data)
     */
    public Data readNext(Data datum) throws Exception {
        int i = this.choose();
        return streams.get(i).read();
    }

    public static void main(String[] args) throws Exception {
        MixedStream ms = new MixedStream();
        ms.readNext();
    }

    /**
     * @see stream.io.DataStream#close()
     */
    @Override
    public void close() {
    }
}