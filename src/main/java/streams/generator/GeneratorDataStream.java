package streams.generator;

import java.util.ArrayList;
import java.util.List;

import stream.Processor;
import stream.io.AbstractStream;

public abstract class GeneratorDataStream extends AbstractStream {

    final List<Processor> processors = new ArrayList<Processor>();

    public void addPreprocessor(Processor proc) {
        processors.add(proc);
    }

    public void addPreprocessor(int idx, Processor proc) {
        processors.add(idx, proc);
    }

    public List<Processor> getPreprocessors() {
        return processors;
    }

}
