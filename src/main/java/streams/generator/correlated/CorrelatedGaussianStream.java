package streams.generator.correlated;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.random.CorrelatedRandomVectorGenerator;
import org.apache.commons.math3.random.GaussianRandomGenerator;
import org.apache.commons.math3.random.JDKRandomGenerator;

import stream.Data;
import stream.data.DataFactory;
import stream.generator.ProportionalOracle;
import stream.io.Stream;

/**
 * Generator stream with covariance matrix specifying correlation between
 * features. For each class label, a different covariance structure is
 * specified. Moreover, the class proportion is set.
 * 
 * @author mbunse
 */
public class CorrelatedGaussianStream implements Stream {
	
	/**
	 * Mean and covariance structure for one class label, together with class
	 * proportion.
	 */
	public static class ClassStructure {
		
		protected double weight;	// class proportion
		protected double[] mean;	// mean along each dimension
		protected RealMatrix cov;	// covariance matrix
		
		public ClassStructure(double weight, double[] mean, RealMatrix cov) {
			this.weight = weight;
			this.mean = mean;
			this.cov = cov.copy();
		}
		
	}
	
	private String id;
	private Long limit;
	
	private long seed = System.currentTimeMillis();	// seed for random seed generator
	private Map<String, ClassStructure> structs;	// structure of each class (key is label)
	
	private Random seedGen;							// seed generator
	private ProportionalOracle classOracle;			// random class selector
	private Map<String, CorrelatedRandomVectorGenerator> gen;	// random vector generator of each class
	private int numItems = 0;						// number of items generated so far

	@Override
	public void init() throws Exception {

		this.seedGen = new Random(seed);	// init seed generator
		
		// init class oracle
		this.classOracle = new ProportionalOracle(seedGen.nextLong());

		// create vector generator for each label
		gen = new HashMap<>();
		Map<String, Double> proportions = new HashMap<>();	// required for class oracle
		for (String label : structs.keySet()) {
			ClassStructure s = structs.get(label);
			gen.put(label, new CorrelatedRandomVectorGenerator(
					s.mean, s.cov, 0d,
					new GaussianRandomGenerator(new JDKRandomGenerator(seedGen.nextInt()))));
			proportions.put(label, s.weight);		// required for class oracle
		}
		
		this.classOracle.setWeights(proportions);
		
	}

	@Override
	public Data read() throws Exception {
		
		Data item = DataFactory.create();
		
		// generate random label
		String label = this.classOracle.getNext();
		item.put("@label", label);
		
		// generate random vector of label
		double[] vec = this.gen.get(label).nextVector();
		for (int i = 0; i < vec.length; i++)
			item.put("att" + String.format("%02d", i+1), vec[i]);
			
		this.numItems++;
		return item;
		
	}

	@Override
	public void close() throws Exception {
		this.numItems = 0;
	}

	/**
	 * @return Root seed used to generate other random number generator seeds.
	 */
	public long getSeed() {
		return this.seed;
	}

	/**
	 * @param seed
	 *            Root seed used to generate other random number generator
	 *            seeds.
	 */
	public void setSeed(long seed) {
		this.seed = seed;
	}

	/**
	 * @return Mapping from class labels to their structure (mean, covariance
	 *         and proportion).
	 */
	public Map<String, ClassStructure> getClassStructures() {
		return Collections.unmodifiableMap(this.structs);
	}

	/**
	 * @param structs
	 *            Mapping from class labels to their structure (mean, covariance
	 *            and proportion).
	 */
	public void setClassStructures(Map<String, ClassStructure> structs) {
		this.structs = structs;
	}
	
	/**
	 * @return The number of items generated so far
	 */
	public int getNumItems() {
		return this.numItems;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Long getLimit() {
		return this.limit;
	}

	@Override
	public void setLimit(Long limit) {
		this.limit = limit;
	}

}
