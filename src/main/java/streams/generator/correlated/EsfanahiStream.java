package streams.generator.correlated;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates the synthetic data specified and used by Esfanahi et al. Always
 * only generates one of the two classes (0 or 1).
 * 
 * @see Esfahani, Mohammad Shahrokh, and Edward R. Dougherty.
 *      "Effect of separate sampling on classification accuracy." Bioinformatics
 *      (2013).
 * @author mbunse
 */
public class EsfanahiStream extends CorrelatedGaussianStream {
	
	private final static Logger log = LoggerFactory.getLogger(EsfanahiStream.class);
	
	private boolean equalCov;
	private int label;

	/**
	 * @param equalCov Iff true, the covariance matrices of both classes are identical
	 * @param label The class to generate. Can be 0 or 1.
	 */
	public EsfanahiStream(boolean equalCov, int label) {
		super();
		if (label != 0 && label != 1)
			throw new IllegalArgumentException("RTFM");
		this.equalCov = equalCov;
		this.label = label;
	}

	@Override
	public void init() throws Exception {
		
		// set mean vector depending on class
		double[] mean = this.setupMean();
		
		// set correlation depending on equalCov
		double sigmaSquare = this.setupSigmaSquare();
		double rho = 0.8;
		double corr = sigmaSquare * rho;
		RealMatrix cov = new Array2DRowRealMatrix(15, 15);
		for (int block = 0; block < 3; block++) {	// block index
			int offset = block * 5;
			for (int row = 0; row < 5; row++)		// fill block
				for (int col = 0; col < 5; col++)
					cov.setEntry(offset+row, offset+col,
							(col == row) ? sigmaSquare : corr);	// set diagonal or correlation entry
		}
		log.debug("Correlation matrix (label {}):\n{}", this.label, cov.toString());
		
		Map<String, ClassStructure> structs = new HashMap<>();
		structs.put(Integer.toString(this.label), new ClassStructure(1d, mean, cov));
		this.setClassStructures(structs);
		
		super.init();	// init random generator
		
	}
	
	/**
	 * Set up the mean vector for the specified class
	 */
	protected double[] setupMean() {
		double[] mean = new double[15];
		for (int i = 0; i < mean.length; i++)
			mean[i] = (this.label == 0) ? 0.3 : 0.8;
		return mean;
	}
	
	/**
	 * Set up the sigmaSquare value for the specified class
	 */
	protected double setupSigmaSquare() {
		return (this.equalCov || this.label == 0) ? 0.4 : 1.6;
	}

	/**
	 * @return The label created by this stream
	 */
	protected int getLabel() {
		return label;
	}
	
}
